# Kronnika Excel Format

## Requirements
* pandas
* xlsxwriter

## Usage
* Call it with one argument directs a folder which contains the excel files.
i.e: python3 kronnikaExcel.py /projects/mert/kr01/excels/results

* It will convert no-formatted excel file into kronnika formatted excel file

