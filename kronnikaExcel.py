import pandas as pd
import xlsxwriter
import sys
import os

class excel:
    def __init__(self):
        self.data = None
        self.workbook = None
        self.worksheet = None
        self.header_format = None
        self.title_format = None
        self.image_format = None
        self.header_list=None
        self.project_name=None
        self.kronnika_logo_path=None
        self.customer_logo_path=None
        self.column_width=None
        self.image_format_kronnika = {'x_scale': 0.2, 'y_scale': 0.2, 'x_offset': -35, 'y_offset': 30}
        self.image_format_customer = {'x_scale': 0.2, 'y_scale': 0.2, 'x_offset': -35, 'y_offset': 30}
    def read_excel(self, path):
        self.data = pd.read_excel(path)
        self.data.fillna("", inplace=True)

    def write_excel(self, path):
        self.workbook = xlsxwriter.Workbook(path)
        self.worksheet = self.workbook.add_worksheet()
        self.excel_styles()
        self.title_row()
        self.header_row()
        self.data = self.data.fillna(0)
        self.fill_data()
        self.workbook.close()

    def title_row(self):
        if self.customer_logo_path!=None:
            self.worksheet.insert_image("A1", self.customer_logo_path, self.image_format_customer)
        if self.kronnika_logo_path!=None:
            self.worksheet.insert_image("F1", self.kronnika_logo_path, self.image_format_kronnika)


    def header_row(self):
        i=0
        for x in self.header_list:
            self.worksheet.write(1, i, x, self.header_format)
            i=i+1

    def fill_data(self):
        for row in range(2, len(self.data) + 2):
            for col in range(len(self.data.columns.values.tolist())):
                self.worksheet.write(row, col, self.data.iloc[row - 2][col], self.cell_format)

    def excel_styles(self):
        self.title_format = self.workbook.add_format(
            {'bold': True, 'align': 'center', 'valign': 'vcenter', 'font_size': 12, 'font_color': '#e33062',
             'bg_color': 'white'})
        self.image_format = {'x_scale': 0.5, 'y_scale': 0.5}
        self.worksheet.set_row(0, 70)
        if self.column_width!=None:
            for x in range(len(self.column_width)):
                self.worksheet.set_column(x, x, self.column_width[x])
        else:
            for x in range(len(self.header_list)):
                self.worksheet.set_column(x, x, 20)
        self.worksheet.merge_range("A1:F1", self.project_name, self.title_format)
        # Header
        self.header_format = self.workbook.add_format(
            {'bold': True, 'align': 'center', 'valign': 'vcenter', 'bg_color': '#e33062', 'font_color': 'white',
             'bottom': 1, 'top': 1, 'left': 1, 'right': 1})
        # Cell
        self.cell_format = self.workbook.add_format(
            {'bold': False, 'align': 'center', 'valign': 'vcenter', 'bg_color': 'white', 'font_color': 'black', 'bottom': 1,
             'top': 1, 'left': 1, 'right': 1})

# return if argument number is not 2
if (len(sys.argv) != 2 ):
	print("Usage:", sys.argv[0], "<folder contains excel files>")
	sys.exit()

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path);
files = os.listdir(sys.argv[1])
excelProcess = excel()
for i in files:
	if i.endswith('.xlsx'):
		full_path = str(sys.argv[1]) + "/" + i
		# After converting we call our kronnika excel class
		excelProcess.read_excel(full_path)
		#headerTitles=["cc","bb","aa","dd","ee","nn"]
		excelProcess.header_list=excelProcess.data.columns
		# We add our project name for Main title
		excelProcess.project_name="Project Name"
		# We add and scale our's and customer's logo
		excelProcess.kronnika_logo_path='kronnika_logo.png'
		#eklenen logoya göre boyut ayarlamas? yap?lmal?
		excelProcess.image_format_customer={'x_scale': 0.3, 'y_scale': 0.3, 'x_offset': -35, 'y_offset': 30}
		excelProcess.image_format_kronnika={'x_scale': 0.2, 'y_scale': 0.2, 'x_offset': -35, 'y_offset': 30}
		excelProcess.customer_logo_path=None#Mü?teri logosu eklenecek buraya
		# We set our column width if we leave it blank it will be 20
		excelProcess.column_width=[10,20,30,40,15,20]#Bo? b?rak?l?rsa bütün geni?likler 20 olarak al?n?r
		# Finally, we run our function with path and xlsx name
		excelProcess.write_excel(full_path)
print("success\n")
